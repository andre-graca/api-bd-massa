package br.com.rsinet.api_bd_massa.codigo_barras;

import java.util.List;

/**
 * Classe para controlar os acessos ao banco de dados para o c�digo de barras
 * 
 * @author ANDRE
 *
 * 
 */
public class CodigoBarrasBuilder {

	/**
	 * 
	 * @return - Lista com todos os c�digos de barra salvos no banco de dados.
	 */
	public List<CodigoBarras> listarTodosCodigosBarra() {
		return new CodigoBarrasDAO().listar();
	}

	/**
	 * Busca o c�digo de barras pelo ID
	 * 
	 * @param ID
	 *            ID do c�digo de barras pesquisado
	 * @return - CodigoBarras pesquisado pelo ID
	 */
	public CodigoBarras buscaCodigoBarras(String ID) {
		return new CodigoBarrasDAO().buscarPorProprieadeString("idCodigoBarras", ID);
	}

}
