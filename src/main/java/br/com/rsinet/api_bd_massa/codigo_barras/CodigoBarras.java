package br.com.rsinet.api_bd_massa.codigo_barras;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/***
 * Classe para representar os c�digos de barra da URA
 * 
 * @author ANDRE
 * 
 * 
 */
@Entity
public class CodigoBarras {

	public CodigoBarras() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ID;

	private String idCodigoBarras;

	private String tipo;

	@Lob
	private String codigoBarras;

	private String valor;

	private Date dataVencimento;

	/**
	 * 
	 * @return - ID do c�digo de barras
	 */
	public String getIdCodigoBarras() {
		return idCodigoBarras;
	}

	/**
	 * 
	 * @return - Tipo do c�digo de barras
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * 
	 * @return - N�mero do c�digo de barras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * 
	 * @return - Valor do c�digo de barras
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * 
	 * @return - Data de vencimento no formato dd/MM/yyyy
	 */
	public String getDataVencimento() {
		return dataVencimento == null ? null : new SimpleDateFormat("dd/MM/yyyy").format(dataVencimento);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((codigoBarras == null) ? 0 : codigoBarras.hashCode());
		result = prime * result + ((dataVencimento == null) ? 0 : dataVencimento.hashCode());
		result = prime * result + ((idCodigoBarras == null) ? 0 : idCodigoBarras.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodigoBarras other = (CodigoBarras) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (codigoBarras == null) {
			if (other.codigoBarras != null)
				return false;
		} else if (!codigoBarras.equals(other.codigoBarras))
			return false;
		if (dataVencimento == null) {
			if (other.dataVencimento != null)
				return false;
		} else if (!dataVencimento.equals(other.dataVencimento))
			return false;
		if (idCodigoBarras == null) {
			if (other.idCodigoBarras != null)
				return false;
		} else if (!idCodigoBarras.equals(other.idCodigoBarras))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CodigoBarras [idCodigoBarras=" + idCodigoBarras + ", tipo=" + tipo + ", codigoBarras=" + codigoBarras
				+ ", valor=" + valor + ", dataVencimento=" + dataVencimento + "]";
	}

}
