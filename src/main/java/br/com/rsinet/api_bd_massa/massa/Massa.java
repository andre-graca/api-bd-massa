package br.com.rsinet.api_bd_massa.massa;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Classe para representar as massas da URA
 * 
 * @author ANDRE
 * 
 * 
 *
 */
@Entity
public class Massa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ID;

	private String IdMassa;

	private String cpf;

	private Date dataNascimento;

	private String agencia;

	private String contaCorrente;

	private String contaPoupanca;

	private String numeroCartao;

	private String cvv;

	private String senhaQuatroDigitos;

	private String senhaSeisDigitos;

	private boolean cso;

	private String agenciaFavorecido;

	private String contaFavorecido;

	private String cpfFavorecido;

	private String numeroTalaoCheques;

	private String assinaturaEletronica;

	@Lob
	private String problema;

	/**
	 * 
	 * @return - ID da Massa
	 */
	public String getIdMassa() {
		return IdMassa;
	}

	/**
	 * 
	 * @return - CPF do cliente cadastrado na massa
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * 
	 * @return - Data de Nascimento do cliente cadastrado na massa
	 */
	public String getDataNascimento() {
		return dataNascimento == null ? null : new SimpleDateFormat("dd/MM/yyyy").format(dataNascimento);
	}

	/**
	 * 
	 * @return - Ag�ncia do cliente cadastrado na massa
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * 
	 * @return - Conta corrente do cliente cadastrado na massa
	 */
	public String getContaCorrente() {
		return contaCorrente;
	}

	/**
	 * 
	 * @return - Conta poupan�a do cliente cadastrado na massa
	 */
	public String getContaPoupanca() {
		return contaPoupanca;
	}

	/**
	 * 
	 * @return - N�mero do cart�o do cliente cadastrado na massa
	 */
	public String getNumeroCartao() {
		return numeroCartao;
	}

	/**
	 * 
	 * @return - CVV do cliente cadastrado na massa
	 */
	public String getCvv() {
		return cvv;
	}

	/**
	 * 
	 * @return - Senha de quatro d�gitos do cliente cadastrado na massa
	 */
	public String getSenhaQuatroDigitos() {
		return senhaQuatroDigitos;
	}

	/**
	 * 
	 * @return - Senha de seis d�gitos do cliente cadastrado na massa
	 */
	public String getSenhaSeisDigitos() {
		return senhaSeisDigitos;
	}

	/**
	 * 
	 * @return - Se o cliente cadastrado tem CSO
	 */
	public boolean isCso() {
		return cso;
	}

	/**
	 * 
	 * @return - Ag�ncia do favorecido cadastrado na massa
	 */
	public String getAgenciaFavorecido() {
		return agenciaFavorecido;
	}

	/**
	 * 
	 * @return - Conta do favorecido cadastrado na massa
	 */
	public String getContaFavorecido() {
		return contaFavorecido;
	}

	/**
	 * 
	 * @return - CPF do favorecido cadastrado na massa
	 */
	public String getCpfFavorecido() {
		return cpfFavorecido;
	}

	/**
	 * 
	 * @return - N�mero do Tal�o de Cheques do cliente cadastrado na massa
	 */
	public String getNumeroTalaoCheques() {
		return numeroTalaoCheques;
	}

	/**
	 * 
	 * @return - Assinatura Eletr�nica do cliente cadastrado na massa
	 */
	public String getAssinaturaEletronica() {
		return assinaturaEletronica;
	}

	/**
	 * 
	 * @return - Problema relacionado a massa
	 */
	public String getProblema() {
		return problema;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((IdMassa == null) ? 0 : IdMassa.hashCode());
		result = prime * result + ((agencia == null) ? 0 : agencia.hashCode());
		result = prime * result + ((agenciaFavorecido == null) ? 0 : agenciaFavorecido.hashCode());
		result = prime * result + ((assinaturaEletronica == null) ? 0 : assinaturaEletronica.hashCode());
		result = prime * result + ((contaCorrente == null) ? 0 : contaCorrente.hashCode());
		result = prime * result + ((contaFavorecido == null) ? 0 : contaFavorecido.hashCode());
		result = prime * result + ((contaPoupanca == null) ? 0 : contaPoupanca.hashCode());
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((cpfFavorecido == null) ? 0 : cpfFavorecido.hashCode());
		result = prime * result + (cso ? 1231 : 1237);
		result = prime * result + ((cvv == null) ? 0 : cvv.hashCode());
		result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result + ((numeroCartao == null) ? 0 : numeroCartao.hashCode());
		result = prime * result + ((numeroTalaoCheques == null) ? 0 : numeroTalaoCheques.hashCode());
		result = prime * result + ((problema == null) ? 0 : problema.hashCode());
		result = prime * result + ((senhaQuatroDigitos == null) ? 0 : senhaQuatroDigitos.hashCode());
		result = prime * result + ((senhaSeisDigitos == null) ? 0 : senhaSeisDigitos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Massa other = (Massa) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (IdMassa == null) {
			if (other.IdMassa != null)
				return false;
		} else if (!IdMassa.equals(other.IdMassa))
			return false;
		if (agencia == null) {
			if (other.agencia != null)
				return false;
		} else if (!agencia.equals(other.agencia))
			return false;
		if (agenciaFavorecido == null) {
			if (other.agenciaFavorecido != null)
				return false;
		} else if (!agenciaFavorecido.equals(other.agenciaFavorecido))
			return false;
		if (assinaturaEletronica == null) {
			if (other.assinaturaEletronica != null)
				return false;
		} else if (!assinaturaEletronica.equals(other.assinaturaEletronica))
			return false;
		if (contaCorrente == null) {
			if (other.contaCorrente != null)
				return false;
		} else if (!contaCorrente.equals(other.contaCorrente))
			return false;
		if (contaFavorecido == null) {
			if (other.contaFavorecido != null)
				return false;
		} else if (!contaFavorecido.equals(other.contaFavorecido))
			return false;
		if (contaPoupanca == null) {
			if (other.contaPoupanca != null)
				return false;
		} else if (!contaPoupanca.equals(other.contaPoupanca))
			return false;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (cpfFavorecido == null) {
			if (other.cpfFavorecido != null)
				return false;
		} else if (!cpfFavorecido.equals(other.cpfFavorecido))
			return false;
		if (cso != other.cso)
			return false;
		if (cvv == null) {
			if (other.cvv != null)
				return false;
		} else if (!cvv.equals(other.cvv))
			return false;
		if (dataNascimento == null) {
			if (other.dataNascimento != null)
				return false;
		} else if (!dataNascimento.equals(other.dataNascimento))
			return false;
		if (numeroCartao == null) {
			if (other.numeroCartao != null)
				return false;
		} else if (!numeroCartao.equals(other.numeroCartao))
			return false;
		if (numeroTalaoCheques == null) {
			if (other.numeroTalaoCheques != null)
				return false;
		} else if (!numeroTalaoCheques.equals(other.numeroTalaoCheques))
			return false;
		if (problema == null) {
			if (other.problema != null)
				return false;
		} else if (!problema.equals(other.problema))
			return false;
		if (senhaQuatroDigitos == null) {
			if (other.senhaQuatroDigitos != null)
				return false;
		} else if (!senhaQuatroDigitos.equals(other.senhaQuatroDigitos))
			return false;
		if (senhaSeisDigitos == null) {
			if (other.senhaSeisDigitos != null)
				return false;
		} else if (!senhaSeisDigitos.equals(other.senhaSeisDigitos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Massa [IdMassa=" + IdMassa + ", cpf=" + cpf + ", dataNascimento=" + dataNascimento + ", agencia="
				+ agencia + ", contaCorrente=" + contaCorrente + ", contaPoupanca=" + contaPoupanca + ", numeroCartao="
				+ numeroCartao + ", cvv=" + cvv + ", senhaQuatroDigitos=" + senhaQuatroDigitos + ", senhaSeisDigitos="
				+ senhaSeisDigitos + ", cso=" + cso + ", agenciaFavorecido=" + agenciaFavorecido + ", contaFavorecido="
				+ contaFavorecido + ", cpfFavorecido=" + cpfFavorecido + ", numeroTalaoCheques=" + numeroTalaoCheques
				+ ", assinaturaEletronica=" + assinaturaEletronica + ", problema=" + problema + "]";
	}

}
