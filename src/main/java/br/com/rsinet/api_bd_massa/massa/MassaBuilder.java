package br.com.rsinet.api_bd_massa.massa;

import java.util.List;

/**
 * Classe para representar a massa no banco de dados
 * 
 * @author ANDRE
 *
 * 
 */
public class MassaBuilder {

	/**
	 * 
	 * @return - Lista com todas as massas no banco de dados.
	 */
	public List<Massa> listarTodasMassas() {
		return new MassaDAO().listar();
	}

	/**
	 * Busca a massa pelo ID
	 * 
	 * @param ID
	 *            ID da massa pesquisada
	 * @return - Massa pesquisada pelo ID
	 */
	public Massa buscaMassa(String ID) {
		return new MassaDAO().buscarPorProprieadeString("IdMassa", ID);
	}
}
