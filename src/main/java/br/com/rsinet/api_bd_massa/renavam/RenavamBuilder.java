package br.com.rsinet.api_bd_massa.renavam;

import java.util.List;

/**
 * Classe para representar o Renavam no banco de dados
 * 
 * @author ANDRE
 *
 * 
 */
public class RenavamBuilder {

	/**
	 * 
	 * @return - Lista com todos os renavams no banco de dados.
	 */
	public List<Renavam> listarTodosRenavams() {
		return new RenavamDAO().listar();
	}

	/**
	 * Busca o renavam pelo ID
	 * 
	 * @param ID
	 *            ID do renvam pesquisado
	 * @return - Renavam pesquisado pelo ID
	 */
	public Renavam buscaRenavam(String ID) {
		return new RenavamDAO().buscarPorProprieadeString("idRenavam", ID);
	}
}
