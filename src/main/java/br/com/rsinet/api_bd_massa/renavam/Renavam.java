package br.com.rsinet.api_bd_massa.renavam;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Classe para representar os renavams da URA
 * 
 * @author ANDRE
 * 
 * 
 */
@Entity
public class Renavam {

	public Renavam() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ID;

	private String idRenavam;
	@Lob
	private String renavam1;
	@Lob
	private String renavam2;
	@Lob
	private String comentarios;

	/**
	 * 
	 * @return - ID do renavam
	 */
	public String getIdRenavam() {
		return idRenavam;
	}

	/**
	 * 
	 * @return - Primeiro renavam
	 */
	public String getRenavam1() {
		return renavam1;
	}

	/**
	 * 
	 * @return - Segundo renavam
	 */
	public String getRenavam2() {
		return renavam2;
	}

	/**
	 * 
	 * @return - Comentários relacionado ao Renavam
	 */
	public String getComentarios() {
		return comentarios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((comentarios == null) ? 0 : comentarios.hashCode());
		result = prime * result + ((renavam1 == null) ? 0 : renavam1.hashCode());
		result = prime * result + ((renavam2 == null) ? 0 : renavam2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Renavam other = (Renavam) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (comentarios == null) {
			if (other.comentarios != null)
				return false;
		} else if (!comentarios.equals(other.comentarios))
			return false;
		if (renavam1 == null) {
			if (other.renavam1 != null)
				return false;
		} else if (!renavam1.equals(other.renavam1))
			return false;
		if (renavam2 == null) {
			if (other.renavam2 != null)
				return false;
		} else if (!renavam2.equals(other.renavam2))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Renavam [idRenavam=" + idRenavam + ", renavam1=" + renavam1 + ", renavam2=" + renavam2
				+ ", comentarios=" + comentarios + "]";
	}

}
