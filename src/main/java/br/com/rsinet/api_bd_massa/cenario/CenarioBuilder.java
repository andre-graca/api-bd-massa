package br.com.rsinet.api_bd_massa.cenario;

import java.util.List;

/**
 * Classe para controlar os acessos ao banco de dados para o cen�rio
 * 
 * @author ANDRE
 *
 * 
 */
public class CenarioBuilder {

	/**
	 * 
	 * @return - Lista com todos os cen�rios salvos no banco de dados.
	 */
	public List<Cenario> listarTodosCenarios() {
		return new CenarioDAO().listar();
	}

	/**
	 * Busca o cen�rio pelo ID
	 * 
	 * @param ID
	 *            ID do cen�rio pesquisado
	 * @return - Cenario pesquisado pelo ID
	 */
	public Cenario buscaCenario(String ID) {
		return new CenarioDAO().buscarPorProprieadeString("idCenario", ID);
	}

}
