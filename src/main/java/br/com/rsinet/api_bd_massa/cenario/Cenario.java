package br.com.rsinet.api_bd_massa.cenario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import br.com.rsinet.api_bd_massa.codigo_barras.CodigoBarras;
import br.com.rsinet.api_bd_massa.codigo_barras.CodigoBarrasBuilder;
import br.com.rsinet.api_bd_massa.massa.Massa;
import br.com.rsinet.api_bd_massa.massa.MassaBuilder;
import br.com.rsinet.api_bd_massa.renavam.Renavam;
import br.com.rsinet.api_bd_massa.renavam.RenavamBuilder;

/**
 * Classe para representar os cen�rios da URA
 * 
 * @author ANDRE
 * 
 * 
 *
 */
@Entity
public class Cenario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ID;

	@Column(length = 8)
	private String idCenario;

	@Lob
	private String cenario;

	private String IdMassa;

	private String IdCodigoBarras;

	private String idRenavam;

	private String projeto;

	/***
	 * 
	 * @return - O ID do cen�rio
	 */
	public String getIdCenario() {
		return idCenario;
	}

	/**
	 * 
	 * @return - A descri��o do cen�rio
	 */
	public String getCenario() {
		return cenario;
	}

	/**
	 * 
	 * @return - A massa relacionada ao cen�rio
	 */
	public Massa massa() {
		return new MassaBuilder().buscaMassa(this.IdMassa);
	}

	/***
	 * 
	 * @return - O c�digo de barras relacionado ao cen�rio
	 */
	public CodigoBarras codigoBarras() {
		return new CodigoBarrasBuilder().buscaCodigoBarras(this.IdCodigoBarras);
	}

	/**
	 * 
	 * @return - O Renavam relacionado ao cen�rio
	 */
	public Renavam renavam() {
		return new RenavamBuilder().buscaRenavam(this.idRenavam);
	}

	/**
	 * 
	 * @return - O projeto relacionado ao cen�rio
	 */
	public String getProjeto() {
		return projeto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((IdCodigoBarras == null) ? 0 : IdCodigoBarras.hashCode());
		result = prime * result + ((IdMassa == null) ? 0 : IdMassa.hashCode());
		result = prime * result + ((cenario == null) ? 0 : cenario.hashCode());
		result = prime * result + ((idCenario == null) ? 0 : idCenario.hashCode());
		result = prime * result + ((idRenavam == null) ? 0 : idRenavam.hashCode());
		result = prime * result + ((projeto == null) ? 0 : projeto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cenario other = (Cenario) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (IdCodigoBarras == null) {
			if (other.IdCodigoBarras != null)
				return false;
		} else if (!IdCodigoBarras.equals(other.IdCodigoBarras))
			return false;
		if (IdMassa == null) {
			if (other.IdMassa != null)
				return false;
		} else if (!IdMassa.equals(other.IdMassa))
			return false;
		if (cenario == null) {
			if (other.cenario != null)
				return false;
		} else if (!cenario.equals(other.cenario))
			return false;
		if (idCenario == null) {
			if (other.idCenario != null)
				return false;
		} else if (!idCenario.equals(other.idCenario))
			return false;
		if (idRenavam == null) {
			if (other.idRenavam != null)
				return false;
		} else if (!idRenavam.equals(other.idRenavam))
			return false;
		if (projeto == null) {
			if (other.projeto != null)
				return false;
		} else if (!projeto.equals(other.projeto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cenario [idCenario=" + idCenario + ", cenario=" + cenario + ", IdMassa=" + IdMassa + ", IdCodigoBarras="
				+ IdCodigoBarras + ", idRenavam=" + idRenavam + ", projeto=" + projeto + "]";
	}

}
