package br.com.rsinet.api_bd_massa.util;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

/**
 * Classe que criar um DAO via Generics
 * 
 * @author ANDRE
 * 
 * 
 * @param <Entidade>
 *            Classe passada via Generics para obten��o do GenericDAO -
 */
public class GenericDAO<Entidade> implements Serializable {

	private static final long serialVersionUID = 753739459078796128L;
	private Class<Entidade> classe;

	// Construtor
	@SuppressWarnings("unchecked")
	public GenericDAO() {
		// API Reflection
		this.classe = (Class<Entidade>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];

	}

	/**
	 * 
	 * @return - Retorna uma lista de objetos
	 */
	@SuppressWarnings("unchecked")
	public List<Entidade> listar() {
		Session sessao = HibernateUtil.getFabricadeSessoes().openSession();
		try {
			Criteria consulta = sessao.createCriteria(classe);
			List<Entidade> resultado = consulta.list();
			return resultado;

		} catch (RuntimeException erro) {
			throw erro;
		} finally {
			sessao.close();
		}

	}

	/**
	 * 
	 * @param nomePropriedade
	 *            nomePropriedade a ser localizada
	 * @param valor
	 *            valor do tipo String
	 * @return - Retorna um Objeto do tipo pesquisado
	 */
	@SuppressWarnings("unchecked")
	public Entidade buscarPorProprieadeString(String nomePropriedade, String valor) {
		Session sessao = HibernateUtil.getFabricadeSessoes().openSession();
		try {
			Criteria consulta = sessao.createCriteria(classe);
			consulta.add(Restrictions.like(nomePropriedade, valor, MatchMode.ANYWHERE));
			Entidade resultado = consulta != null ? (Entidade) consulta.list().get(0) : null;
			return resultado;

		} catch (RuntimeException erro) {
			throw erro;
		} finally {
			sessao.close();
		}
	}

}
